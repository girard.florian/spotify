#!/bin/sh

result=`ps aux | grep -i "script.sh" | grep -v "grep" | wc -l`
if [ $result -ge 1 ]
   then
        echo "script is running"
        exit
   else
        echo "script is not running"
fi

# BASH 
# pids=($(pidof -x on-off.sh))
# if [ ${#pids[@]} -gt 1 ] ; then 
#  echo "Script already running by pid ${pids[1]}" 
#  exit 
# fi

echo "##########################"
echo "# SPOTIFY SCRIPT INIT"
export URL_SCRIPT

echo "# Recorded Event: '$PLAYER_EVENT'";

if [ "$PLAYER_EVENT" = "start" ]; then
  echo "# amplifier on"
elif [ "$PLAYER_EVENT" = "playing" ]; then
  echo "# amplifier on"
elif [ "$PLAYER_EVENT" = "stop" ]; then
  echo "# amplifier off"
elif [ "$PLAYER_EVENT" = "paused" ]; then
  echo "# amplifier off"
fi
echo "##########################"
echo ""
